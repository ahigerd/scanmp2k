HEADERS = $(wildcard *.h)
SOURCES = $(wildcard *.cpp)
OBJS = $(patsubst %.cpp,%.o,$(SOURCES))

scanmp2k: $(OBJS)
	$(CXX) -o $@ $^

%.o: %.cpp $(HEADERS)
	$(CXX) -c -o $@ $< -std=gnu++17 -g -Og

clean: FORCE
	rm -f scanmp2k $(OBJS)

FORCE:
