#include "instrumentdata.h"
#include "romfile.h"
#include "utility.h"
#include <sstream>
#include <cmath>

/*
class SweepNode : public AudioNode
{
public:
  SweepNode(const SynthContext* ctx, AudioNode* source, uint8_t sweep)
  : AudioNode(ctx), source(source), lastTime(0), active(true)
  {
    freq = source->param(BaseOscillator::Frequency).get();
    if (!freq) {
      active = false;
      source = nullptr;
      return;
    }
    sweepShift = sweep & 0x7;
    sweepNegate = sweep & 0x8;
    sweepPeriod = (sweep >> 4) & 0x7;
    if (!sweepShift || !sweepPeriod) {
      sweepShift = 0;
      sweepPeriod = 0;
    }
  }

  bool isActive() const {
    return active;
  }

protected:
  int16_t generateSample(double time, int channel) {
    int16_t sample = source->getSample(time, channel);
    if (!sweepShift) {
      return sample;
    }
    double delta = (time - lastTime) * 128.0;
    if (delta >= sweepPeriod) {
      int16_t timer = 4194304.0 / freq->valueAt(time);
      int16_t adj = timer >> sweepShift;
      timer = sweepNegate ? timer - adj : timer + adj;
      if (timer <= 0) {
        sweepShift = 0;
      } else if (timer >= 2048) {
        active = false;
      } else {
        freq->setConstant(4194304.0 / timer);
      }
      delta -= sweepPeriod;
      lastTime = time - delta;
    }
    return sample;
  }

  AudioNode* source;
  AudioParam* freq;

  double lastTime;
  uint8_t sweepPeriod;
  uint8_t sweepShift;
  bool sweepNegate : 1;
  bool active : 1;
  bool squelch : 1;
};
*/

MpInstrument* MpInstrument::load(const ROMFile* rom, uint32_t addr, bool isSplit)
{
  if (addr == 0x80808080) {
    return nullptr;
  }
  uint8_t type = rom->read<uint8_t>(addr);
  uint8_t normType = type & 0x7 ? type & 0x7 : type;
  try {
    switch (normType) {
      case Sample:
      case GBSample:
      case FixedSample:
        return new SampleInstrument(rom, addr);
      case Square1:
      case Square2:
      case Noise:
        return new PSGInstrument(rom, addr);
      case KeySplit:
      case Percussion:
        if (!isSplit) {
          return new SplitInstrument(rom, addr);
        }
      default:
        return new MpInstrument(rom, addr);
    }
  } catch (ROMFile::BadAccess& e) {
    //std::cerr << "Bad pointer loading " << int(type) << " instrument at 0x" << std::hex << addr << " (0x" << e.addr << ")" << std::dec << std::endl;
    return nullptr;
  } catch (std::exception& e) {
    //std::cerr << "Error loading " << int(type) << " instrument at 0x" << std::hex << addr << std::dec << ": " << e.what() << std::endl;
    return nullptr;
  }
}

MpInstrument::MpInstrument(const ROMFile* rom, uint32_t addr)
: rom(rom), addr(addr), type(Type(rom->read<uint8_t>(addr))), rawType(rom->read<uint8_t>(addr)), forcePan(false), pan(0), gate(0)
{
  if (type & 0x7) {
    type = Type(type & 0x7);
    attack = (rom->read<uint8_t>(addr + 8) & 0x7) / 7.0;
    decay = rom->read<uint8_t>(addr + 9) / 60.0;
    sustain = rom->read<uint8_t>(addr + 10) / 15.0;
    release = rom->read<uint8_t>(addr + 11) / 60.0;
    gate = rom->read<uint8_t>(addr + 2) / 255.0;
  } else if (type == 0 || type == 8) {
    attack = (255 - rom->read<uint8_t>(addr + 8)) / 60.0;
    decay = rom->read<uint8_t>(addr + 9) / 256.0;
    sustain = rom->read<uint8_t>(addr + 10) / 255.0;
    release = rom->read<uint8_t>(addr + 11) / 256.0;
  } else {
    type = Unknown;
  }
}

SampleInstrument::SampleInstrument(const ROMFile* rom, uint32_t addr)
: MpInstrument(rom, addr)
{
  sampleAddr = rom->readPointer(addr + 4);
  sampleBits = 4;
  sampleLen = 16;
  loopStart = 0;
  loopEnd = 32;
  sampleRate = rom->sampleRate;
  uint32_t sampleStart = sampleAddr;
  if (type == GBSample) {
    sampleRate = 4186.0;
  } else {
    pan = rom->read<uint32_t>(addr + 3) ^ 0x80;
    forcePan = !(pan & 0x80);
    if (pan == 127) {
      // adjust full right panning to make centering easier
      pan = 128;
    }
    sampleBits = 8;
    sampleLen = rom->read<uint32_t>(sampleAddr + 12);
    if (rom->read<uint32_t>(sampleAddr)) {
      loopStart = rom->read<uint32_t>(sampleAddr + 8);
      loopEnd = sampleLen;
    } else {
      loopEnd = 0;
    }
    if (type == Sample) {
      sampleRate = rom->read<uint32_t>(sampleAddr + 4) / 1024.0;
    }
    sampleStart = sampleAddr + 16;
  }
  if (sampleStart + sampleLen > rom->rom.size()) {
    throw ROMFile::BadAccess(sampleStart + sampleLen);
  }
}

PSGInstrument::PSGInstrument(const ROMFile* rom, uint32_t addr)
: MpInstrument(rom, addr), sweep(0)
{
  if (type == Square1) {
    sweep = rom->read<uint8_t>(addr + 3);
  }
  mode = rom->read<uint8_t>(addr + 4);
}

SplitInstrument::SplitInstrument(const ROMFile* rom, uint32_t addr)
: MpInstrument(rom, addr)
{
  uint32_t splitAddr = rom->readPointer(0x08000000 | (addr + 4));
  if (type == Percussion) {
    for (int i = 0; i < 128; i++) {
      splits.emplace_back(load(rom, splitAddr + 12 * i, true));
    }
  } else {
    uint32_t tableAddr = rom->readPointer(addr + 8);
    for (int i = 0; i < 128; i++) {
      int n = rom->read<uint8_t>(tableAddr + i);
      splits.emplace_back(load(rom, splitAddr + 12 * n, true));
    }
  }
}

InstrumentData::InstrumentData(const ROMFile* rom, uint32_t addr)
{
  while (addr < rom->rom.size() && instruments.size() < 127) {
    MpInstrument* inst = MpInstrument::load(rom, addr);
    if (inst) {
      instruments.emplace_back(inst);
    } else {
      //std::cerr << "unknown/bad" << std::endl;
      instruments.emplace_back(nullptr);
    }
    addr += 12;
  }
}
