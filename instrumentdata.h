#ifndef GBAMP2WAV_INSTRUMENTDATA_H
#define GBAMP2WAV_INSTRUMENTDATA_H

#include <vector>
#include <cstdint>
#include <memory>
class ROMFile;

class MpInstrument {
public:
  static MpInstrument* load(const ROMFile* rom, uint32_t addr, bool isSplit = false);
  MpInstrument(const ROMFile* rom, uint32_t addr);
  virtual ~MpInstrument() {}

  const ROMFile* rom;
  uint32_t addr;

  enum Type {
    Sample = 0,
    Square1 = 1,
    Square2 = 2,
    GBSample = 3,
    Noise = 4,
    FixedSample = 8,
    KeySplit = 0x40,
    Percussion = 0x80,
    Unknown = 0xFF,
  };
  Type type;
  int rawType;

  double attack, decay, sustain, release;
  bool forcePan;
  uint8_t pan;
  double gate;
};

class SampleInstrument : public MpInstrument {
public:
  SampleInstrument(const ROMFile* rom, uint32_t addr);

  uint32_t sampleAddr;
  int sampleBits, sampleLen, loopStart, loopEnd, sampleRate;
};

class PSGInstrument : public MpInstrument {
public:
  PSGInstrument(const ROMFile* rom, uint32_t addr);

  uint8_t mode, sweep;
};

class SplitInstrument : public MpInstrument {
public:
  SplitInstrument(const ROMFile* rom, uint32_t addr);

  std::vector<std::shared_ptr<MpInstrument>> splits;
};

class InstrumentData {
public:
  InstrumentData(const ROMFile* rom, uint32_t addr);

  std::vector<std::shared_ptr<MpInstrument>> instruments;
};

#endif
