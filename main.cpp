#include "romfile.h"
#include "songtable.h"
#include "songdata.h"
#include "instrumentdata.h"
#include "utility.h"
#include <map>
#include <set>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <memory>
#include <cstdlib>
#include <sstream>

static const std::set<std::string> commonOpcodes {
  "VOICE",
  "TEMPO",
  "FINE",
  "GOTO",
  "PAN",
  "VOL",
  "PRIO",
  "KEYSH",
};

void scanFile(const std::filesystem::path& path)
{
  static bool first = true;
  ROMFile rom;
  uint8_t buffer[1024];
  std::ifstream f(path);
  while (f) {
    f.read(reinterpret_cast<char*>(buffer), sizeof(buffer));
    rom.rom.insert(rom.rom.end(), buffer, buffer + f.gcount());
  }
  SongTable st(rom.findAllSongs());
  if (!st.songs.size()) {
    return;
  }

  bool hasSongs = false;
  for (uint32_t songAddr : st.songs) {
    try {
      if (!rom.checkSong(songAddr, false)) {
        continue;
      }
      SongData* song = st.songAt(songAddr);
      if (!song || !song->tracks.size()) {
        continue;
      }
      hasSongs = true;
    } catch (ROMFile::BadAccess e) {
      continue;
    }
  }
  if (!hasSongs) {
    return;
  }

  if (first) {
    first = false;
  } else {
    std::cout << "," << std::endl;
  }
  std::cout << "{ \"file\": " << path.filename() << "," << std::endl;
  {
    SongTable stTable(rom.findSongTable(-1));
    std::cout << "  \"table\": \"0x" << std::hex << stTable.tableStart << std::dec << "\"," << std::endl;
    std::cout << "  \"entries\": " << stTable.songs.size() << "," << std::endl;
  }

  std::map<uint32_t, const MpInstrument*> insts;

  std::cout << "  \"songs\": {" << std::endl;
  uint32_t last = st.songs.back();
  for (uint32_t songAddr : st.songs) {
    if (!rom.checkSong(songAddr, false)) {
      continue;
    }
    SongData* song;
    try {
      song = st.songAt(songAddr);
    } catch (ROMFile::BadAccess e) {
      continue;
    }
    if (!song || !song->tracks.size()) {
      continue;
    }
    std::string instJson;
    for (uint8_t instID : song->usedInstruments) {
      if (!instJson.empty()) {
        instJson += ", ";
      }
      MpInstrument* inst = song->getInstrument(instID);
      if (!inst) {
        instJson += "\"INVALID\"";
        continue;
      }
      std::stringstream ss;
      ss << "\"0x" << std::hex << inst->addr << "\"";
      instJson += ss.str();
      if (!insts.count(inst->addr)) {
        insts[inst->addr] = inst;
      }
    }
    std::string opJson;
    for (const auto& iter : song->opcodes) {
      if (commonOpcodes.count(iter.first)) continue;
      if (!opJson.empty()) {
        opJson += ", ";
      }
      opJson += "\"" + iter.first + "\": " + std::to_string(iter.second);
    }
    std::cout << "    \"0x" << std::hex << songAddr << std::dec << "\": {" << std::endl;
    if (!instJson.empty()) {
      std::cout << "      \"instruments\": [ " << instJson << " ]," << std::endl;
    }
    if (!opJson.empty()) {
      std::cout << "      \"opcodes\": { " << opJson << " }," << std::endl;
    }
    if (song->hasNested) {
      std::cout << "      \"error\": \"nested\"," << std::endl;
    }
    std::cout << "      \"tracks\": " << song->tracks.size() << std::endl;
    if (songAddr == last) {
      std::cout << "    }" << std::endl;
    } else {
      std::cout << "    }," << std::endl;
    }
  }
  std::cout << "  }," << std::endl;

  std::cout << "  \"instruments\": {" << std::endl;
  if (!insts.empty()) {
    last = insts.rbegin()->first;
    for (const auto& iter : insts) {
      std::cout << "    \"0x" << std::hex << iter.first << std::dec << "\": {" << std::endl;
      if (iter.second->rawType == MpInstrument::Unknown) {
        std::cout << "      \"unknown\": true," << std::endl;
      } else if (iter.second->rawType & 0x7 == MpInstrument::Square1) {
        auto psg = static_cast<const PSGInstrument*>(iter.second);
        if (psg->sweep) {
          std::cout << "      \"sweep\": true," << std::endl;
        }
      } else if (iter.second->type == MpInstrument::KeySplit || iter.second->type == MpInstrument::Percussion) {
        auto split = static_cast<const SplitInstrument*>(iter.second);
        if (iter.second->type == MpInstrument::Percussion) {
          std::cout << "      \"percussion\": true," << std::endl;
        }
        std::cout << "      \"splits\": " << split->splits.size() << "," << std::endl;
      }
      if (iter.second->type == MpInstrument::Sample || iter.second->type == MpInstrument::FixedSample || iter.second->type == MpInstrument::GBSample) {
        auto sample = static_cast<const SampleInstrument*>(iter.second);
        std::cout << "      \"sample\": \"0x" << std::hex << sample->sampleAddr << std::dec << "\"," << std::endl;
      }
      std::cout << "      \"type\": \"0x" << std::hex << std::setw(2) << std::setfill('0') << int(iter.second->rawType) << std::dec << "\"" << std::endl;
      if (iter.first == last) {
        std::cout << "    }" << std::endl;
      } else {
        std::cout << "    }," << std::endl;
      }
    }
  }
  std::cout << "  } }";
}

void recursePath(const std::filesystem::path& path)
{
  std::filesystem::directory_entry ent(path);
  if (ent.is_regular_file()) {
    scanFile(path);
  } else if (ent.is_directory()) {
    for (const auto& child : std::filesystem::directory_iterator(path)) {
      if (child.path().string()[0] == '.') {
        continue;
      }
      recursePath(child);
    }
  }
}

int main(int argc, char** argv)
{
  if (argc < 2 || std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help") {
    std::cerr << "Usage: " << argv[0] << " <path>+" << std::endl;
    std::cerr << std::endl;
    std::cerr << "Outputs JSON to stdout" << std::endl;
    return 1;
  }

  std::cout << "[" << std::endl;
  for (int i = 1; i < argc; i++) {
    recursePath(argv[i]);
  }
  std::cout << std::endl << "]" << std::endl;

  return 0;
}
